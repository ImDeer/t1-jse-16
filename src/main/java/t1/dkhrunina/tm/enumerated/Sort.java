package t1.dkhrunina.tm.enumerated;

import t1.dkhrunina.tm.comparator.CreatedComparator;
import t1.dkhrunina.tm.comparator.NameComparator;
import t1.dkhrunina.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    private final String displayName;

    private final Comparator<?> comparator;

    Sort(final String displayName, final Comparator<?> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static Sort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Sort sort : values()) {
            if (sort.getDisplayName().equals(value)) return sort;
        }
        return null;
    }

    public static String[] toValues() {
        final String[] values = new String[Sort.values().length];
        int index = 0;
        for (Sort sort : Sort.values()) {
            values[index] = sort.getDisplayName();
            index++;
        }
        return values;
    }

    public String getDisplayName() {
        return displayName;
    }

    @SuppressWarnings("rawtypes")
    public Comparator getComparator() {
        return comparator;
    }

}