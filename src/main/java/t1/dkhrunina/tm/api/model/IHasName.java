package t1.dkhrunina.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}