package t1.dkhrunina.tm.service;

import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.api.service.ITaskService;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.exception.entity.TaskNotFoundException;
import t1.dkhrunina.tm.exception.field.DescriptionEmptyException;
import t1.dkhrunina.tm.exception.field.IdEmptyException;
import t1.dkhrunina.tm.exception.field.IndexIncorrectException;
import t1.dkhrunina.tm.exception.field.NameEmptyException;
import t1.dkhrunina.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.add(task);
    }

    @Override
    public Task changeTaskStatusById(String id, Status status) {
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(Integer index, Status status) {
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return taskRepository.create(name);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return taskRepository.create(name, description);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return findAll();
        return taskRepository.findAll(comparator);
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if (sort == null) return findAll();
        final Comparator<Task> comparator = sort.getComparator();
        return findAll(comparator);
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index >= taskRepository.getSize()) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public int getSize() {
        return taskRepository.getSize();
    }

    @Override
    public Task remove(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0 || index >= taskRepository.getSize()) throw new IndexIncorrectException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

}